<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PHP Hello world</title>
  </head>
  <body>
    <? if ($_GET['name'] != '') {?>
      <h1>Hello <? echo htmlspecialchars($_GET['name']); ?></h1>
    <? } else { ?>
      <h1>Hello <? echo 'world'; ?> from php demo..</h1>
    <? } ?>
  </body>
</html>
